package facci.pm.ta2.poo.pra1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Caratula extends AppCompatActivity {

  Button siguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caratula);

        siguiente = (Button)findViewById(R.id.btnIniciar);

        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cambiar = new Intent(Caratula.this,ResultsActivity.class);
                startActivity(cambiar);
            }
        });
    }
}
